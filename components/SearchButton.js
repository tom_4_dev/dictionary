import React, { Component } from 'react';
import { Text, TouchableHighlight } from 'react-native';

class SearchButton extends Component {
  _onPressButton() {
    console.log("you tap  here")
  }

  render() {
    return (
      <TouchableHighlight onPress={this._onPressButton}>
        <Text>Search</Text>
      </TouchableHighlight>
    )
  }
}

export default SearchButton;

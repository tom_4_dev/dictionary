import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, TextInput, View } from 'react-native';
import Button from 'react-native-button';
import SearchButton from './components/SearchButton.js'

class Dictionary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      result_text: '',
      searchText: 'SEARCH'
    }

    this.updateText = this.updateText.bind(this);
    this.searchMeaning = this.searchMeaning.bind(this);
  }

  updateText(e) {
    this.setState({ text: e })

    if(e.length == 0) {
      this.setState({ result_text: '' })
    }
  }

  removeUndefined(value) {
    return value != "undefined";
  }

  searchMeaning() {
    this.setState({ searchText: "SEARCHING..." });
    return fetch('https://glosbe.com/gapi/translate?from=eng&dest=ben&format=json&phrase='+ this.state.text.toLowerCase() +'&pretty=true')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({ searchText: "SEARCH" });
      let results;

      if(responseJson.result == "ok") {
        results = responseJson.tuc.map(function(obj) {
          if(obj.phrase) {
            return obj.phrase.text;
          }
        });
      }
      console.log(results.length);
      if(results.length > 0) {
        this.setState({ result_text: results.join(", ") });
      } else {
        this.setState({ result_text: "Not found! Check your spelling or may be a vulgar word." });
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
    return (
      <View style={styles.main_container}>
        <View style={styles.welcome_container}>
          <Text style={styles.welcome}>
            English to Bengali
          </Text>
        </View>

        <View style={styles.container}>
          <TextInput
            style={styles.english_text}
            placeholder="Type Here in English"
            onChangeText={ (text) => this.updateText(text) }
          />

         <Button
           style={{borderWidth: 1, color: 'white', textAlign: 'center'}}
           containerStyle={{backgroundColor: '#660000', height: 45, borderRadius: 8, paddingTop: 10 }}
           onPress={ this.searchMeaning }>
           { this.state.searchText }
         </Button>

         <Text style={styles.result}>
           { this.state.result_text }
         </Text>
        </View>

        <View style={styles.footer_container}>
          <Text style={{color: 'white'}}>
             2015-16
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'column'
  },

  welcome_container: {
    flex: 1.5,
    backgroundColor: '#660000',
    padding: 20
  },

  container: {
    flex: 10,
    backgroundColor: '#A52A2A',
    padding: 10
  },

  footer_container: {
    flex: 1,
    backgroundColor: '#660000',
    alignItems: 'center',
    padding: 10
  },

  welcome: {
    color: 'aliceblue',
    fontSize: 20,
    textAlign: 'center',
  },

  english_text: {
    alignItems: 'center',
    height: 50,
    fontSize: 20,
    width: 300,
    color: 'aliceblue',
    marginBottom: 20
  },

  result: {
    color: 'white',
    marginTop: 20,
    fontSize: 20
  }
});

AppRegistry.registerComponent('Dictionary', () => Dictionary);
